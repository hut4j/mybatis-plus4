<p align="center">
  <a href="https://github.com/baomidou/mybatis-plus">
   <img alt="Mybatis-Plus-Logo" src="https://raw.githubusercontent.com/baomidou/logo/master/mybatis-plus-logo-new-mini.png">
  </a>
</p>

<p align="center">
  Born To Simplify Development
</p>

<p align="center">
  <a href="https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.baomidou%22%20AND%20a%3A%22mybatis-plus%22">
    <img alt="maven" src="https://img.shields.io/maven-central/v/com.baomidou/mybatis-plus.svg?style=flat-square">
  </a>

  <a href="https://www.apache.org/licenses/LICENSE-2.0">
    <img alt="code style" src="https://img.shields.io/badge/license-Apache%202-4EB1BA.svg?style=flat-square">
  </a>

  <a href="https://gitter.im/baomidou/mybatis-plus?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge">
    <img alt="Join the chat at https://gitter.im/baomidou/mybatis-plus" src="https://badges.gitter.im/baomidou/mybatis-plus.svg">
  </a>
</p>

> 规划计划

![MP4](https://images.gitee.com/uploads/images/2022/0316/222951_c8f6f75a_12260.png "mp4.png")


[官方文档](http://baomidou.com)

[企业版 Mybatis-Mate 高级特性](https://gitee.com/baomidou/mybatis-mate-examples)

### 多表Join支持使用说明
1、准备2个po
 ```java
@Data
@TableName(value="user")
public class User {

    @TableId("user_id")
    private Integer userId;

    @TableField("name")
    private String name;

    @TableField("age")
    private Integer age;

    @TableField("sex")
    private String sex;

	//指定这个字段和School 的id字段关联，如果不是和id字段关联可以使用targetFields属性进行关联
    @TableField(value="school_id",target = School.class)
    private Integer schoolId;

    //指定关联关系，和目标类  如果是  onetoone 则不需要指定target
    @TableField(relation = Relation.ONE_TO_MANY,target = School.class)
    List<School> schools;
}

```
```java
@Data
@TableName(value="school",autoResultMap = true) //这里设置自动生成resultMap 不需要别的操作
public class School {
    @TableId
    private Integer id;

    @TableField("school_name")
    private String schoolName;

    @TableField("remark")
    private String remark;
}

```
2、启动join插件(不然返回结果映射会有问题，此插件动态修改resultMap)        
```java
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new JoinInterceptor());
        return interceptor;
    }
```

3、使用BasicJoinQueryWrapper (为po advance query预留的，不推荐开发者直接使用)  
```java
        BasicJoinQueryWrapper<User> wrapper = new BasicJoinQueryWrapper<>(User.class);
        wrapper.innerJoin(School.class);
        wrapper.eq(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"),1);
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"userId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"name"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"schoolName"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"id"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"remark"));
        mapper.selectList(wrapper);
```
  po advance query写法 ：
  ```java
new User().select(User.SCHOOLID,User.USERID,User.NAME).userId().eq(1).innerJoin(School.class).orgName().like("一").select(School.ID,School.REMARK,School.SCHOOLNAME).list();
```
需要mybatis-plus-advance 依赖：https://gitee.com/baomidou/mybatis-plus-advance

3、使用LambdaJoinQueryWrapper
```java
        LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
        wrapper.eq(User::getSchoolId,1);
        wrapper.innerJoin(School.class).like(School::getSchoolName,"一");
        mapper.selectList(wrapper);
```
### 函数支持
改动：在query接口中添加了selectFun方法，支持用户使用函数来查询字段，目前各类wrapper实现均已支持此查询
```java
 Children selectFun(BaseFuncEnum fun,R alias,R column)
```
1、po上添加接收函数返回的字段
```java
@TableField(exist = false,funField = true) //标记数据库不存在，并且是个函数字段
private Long schoolCount;
```
2、各类wrapper使用
```java
// QueryWrapper
new QueryWrapper<User>().eq("school_id",2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id")

// LambdaQueryWrapper
new LambdaQueryWrapper<User>().eq(User::getSchoolId,2)
                .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount).groupBy(User::getSchoolId)

// QueryChainWrapper
QueryChainWrapper<User> wrapper = new QueryChainWrapper<User>(mapper);
wrapper.eq("school_id",2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id").one()
// LambdaQueryChainWrapper
LambdaQueryChainWrapper<User> wrapper = new LambdaQueryChainWrapper<User>(mapper);
wrapper.eq(User::getSchoolId,2)              .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount).groupBy(User::getSchoolId).one()

// BasicJoinQueryWrapper
 BasicJoinQueryWrapper<User> wrapper = new BasicJoinQueryWrapper<>(User.class);
wrapper.eq(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"),2);
wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));
wrapper.selectFun(DefaultFuncEnum.COUNT,new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolCount"));
wrapper.groupBy(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));

// LambdaJoinQueryWrapper
LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
wrapper.eq(User::getSchoolId,2);
wrapper.select(User::getSchoolId);
wrapper.selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount);
wrapper.groupBy(User::getSchoolId);
```

### 批量插入支持说明
1.目前已经支持的数据库有：mysql,oracle,postgresql,sqlserver
2.序列自增配置：
```java
    /**
     * Postgre 主键生成器
     * @return
     */
    @Bean
    public IKeyGenerator keyGenerator() {
        return new PostgreKeyGenerator(); // oracle和Postgre 序列生成主键都已经支持
    }
    /**
     * 序列自增插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
		//需要依赖此拦截器
        interceptor.addInnerInterceptor(new KeyGeneratorInterceptor());
        return interceptor;
    }
```
3.数据库类型配置(也可以配置到@tablename注解里，注解优先于全局配置)
```yaml
mybatis-plus:
  global-config:
    db-config:
      # 数据库类型
      db-type: mysql
```


### 批量修改支持说明
1.目前已经支持的数据库有：mysql,oracle,postgresql
2.已知问题-oracle返回的受影响行数不对

### 前端高级查询API
后端在参数中配置一个QueryFilter<T>,QueryFilter 提供一个asWrapper方法可以把前端的过滤条件包装为一个QueryWrapper
````java
    @PostMapping("list")
    public List<User> filter(@RequestBody QueryFilter<User> filter){
	//注意sex和age是安全字段，就算前端传了也不会拼接这2个字段的过滤条件。
        return mapper.selectList(filter.asWrapper(User.class,User::getSex,User::getAge));
    }
````
前端按照指定格式传参，即可实现后端不做多余编程前端自由控制过滤条件的效果。
```json
{
   "sorter":[{//排序支持ASC和DESC
       "property":"userId",
       "direction":"DESC"
   }],
   "querys":[{//过滤条件 where sex=男 and (name=张三 or name=李四 )
         "property":"name", // po字段名
         "operator":"=",//操作符 
         "value":"张三",//操作值
         "relation":"OR",//关联关系AND OR
         "group":"nameGroup"//相同的group 外层会加括号
   },
   {
         "property":"name",
         "operator":"=",
         "value":"李四",
         "relation":"OR",
         "group":"nameGroup"
   },{
         "property":"sex", //使用了默认的关联关系AND 以及默认操作符 =
         "value":"男"
   }]
}
```
操作符支持：支持 = > >= < <= like like_l like_r not_like is_null not_null in                                                 not_in between not_between 等常见操作
如果是in 等操作 value支持数组传参
本功能暂定在https://gitee.com/baomidou/mybatis-plus-advance 包中