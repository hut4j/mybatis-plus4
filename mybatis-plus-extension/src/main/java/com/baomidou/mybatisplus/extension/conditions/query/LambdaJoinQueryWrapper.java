/*
 * Copyright (c) 2011-2022, baomidou (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.baomidou.mybatisplus.extension.conditions.query;

import com.baomidou.mybatisplus.core.conditions.AbstractJoinWrapper;
import com.baomidou.mybatisplus.core.conditions.SharedString;
import com.baomidou.mybatisplus.core.enums.BaseFuncEnum;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.*;
import com.baomidou.mybatisplus.core.toolkit.support.LambdaMeta;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import org.apache.ibatis.reflection.property.PropertyNamer;


/**
 * 本类使用SFunction来构造多表查询条件
 * demo:--需要注意的是 如果要添加其他表的过滤条件或者排序字段，请在调用了join方法后直接调用类似下面这种写法，否则编译不通过
 *         LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
 *         wrapper.eq(User::getSchoolId,1);
 *         wrapper.innerJoin(School.class).like(School::getSchoolName,"一");
 *         mapper.selectJoinList(wrapper);
 * @author wanglei
 * @since 2022-03-17
 */
public class LambdaJoinQueryWrapper<T> extends AbstractJoinWrapper<T, SFunction<T, ?>,
    LambdaJoinQueryWrapper<T>> {

    /**
     * 主表model的class
     */
    private Class<?> mainClass;

    public LambdaJoinQueryWrapper(Class<?> mainClass) {
        super(mainClass);
        this.mainClass = mainClass;
        super.initNeed();
        initLogicDelete(mainClass);
    }

    @Override
    protected LambdaJoinQueryWrapper<T> instance() {
        return new LambdaJoinQueryWrapper(mainClass);
    }

    /**
     * inner join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @return this
     */
    public <X> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass) {
        pubJoin(joinClass, Constants.INNER_JOIN);
        return (LambdaJoinQueryWrapper<X>) this;
    }

    /**
     * left join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @return this
     */
    public <X> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass) {
        pubJoin(joinClass, Constants.LEFT_JOIN);
        return (LambdaJoinQueryWrapper<X>) this;
    }

    /**
     * inner join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @return this
     */
    public <X> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass) {
        pubJoin(joinClass, Constants.RIGHT_JOIN);
        return (LambdaJoinQueryWrapper<X>) this;
    }

    /**
     * 软删除字段处理
     * @param joinClass
     */
    @Override
    protected void initLogicDelete(Class<?> joinClass){
        TableInfo tableInfo = TableInfoHelper.getTableInfo(joinClass);
        if(tableInfo.getLogicDeleteFieldInfo()!=null){
            eq(LambdaUtils.getSFunction(joinClass,tableInfo.getLogicDeleteFieldInfo().getPropertyType()
                    ,tableInfo.getLogicDeleteFieldInfo().getProperty())
                ,parseLogicNotDeleteValue(tableInfo.getLogicDeleteFieldInfo()));
        }
    }

    @Override
    protected String columnToString(SFunction<T, ?> column) {
        LambdaMeta lambdaMeta = LambdaUtils.extract(column);
        if (!super.aliasMap.containsKey(lambdaMeta.getInstantiatedClass())) {
            throw ExceptionUtils.mpe("not join class: \"%s\".", lambdaMeta.getInstantiatedClass().getName());
        }
        return super.aliasMap.get(lambdaMeta.getInstantiatedClass()) + "." + getCache(lambdaMeta.getInstantiatedClass(),
            PropertyNamer.methodToProperty(lambdaMeta.getImplMethodName())).getColumn();
    }

    @Override
    public LambdaJoinQueryWrapper<T> select(SFunction<T, ?>... columns) {
        if (ArrayUtils.isNotEmpty(columns)) {
            for (SFunction<?, ?> column : columns) {
                LambdaMeta lambdaMeta = LambdaUtils.extract(column);
                selectColumns.add(SelectColumn.of(lambdaMeta.getInstantiatedClass(), getCache(lambdaMeta.getInstantiatedClass(),
                    PropertyNamer.methodToProperty(lambdaMeta.getImplMethodName())).getColumn()));
            }
        }
        return (LambdaJoinQueryWrapper<T>) typedThis;
    }

    /**
     * 查询函数列,如果column不指定，则查询 *
     * <p>select xx(column) as alias</p>
     */
    public final LambdaJoinQueryWrapper<T> selectFun(BaseFuncEnum fun, SFunction<T, ?> alias, SFunction<T, ?> column) {
        LambdaMeta aliasLambdaMeta = LambdaUtils.extract(alias);
        LambdaMeta columnLambdaMeta = column == null ? null : LambdaUtils.extract(column);
        String columnStr = column == null ? "*" : aliasMap.get(columnLambdaMeta.getInstantiatedClass()) + Constants.DOT
            +   getCache(columnLambdaMeta.getInstantiatedClass(),PropertyNamer.methodToProperty(columnLambdaMeta.getImplMethodName())).getColumn();
        String funStr = String.format(fun.getSql(), columnStr);
        this.funSqlSelect.add(new SharedString(funStr + Constants.AS + PropertyNamer.methodToProperty(aliasLambdaMeta.getImplMethodName())));
        return (LambdaJoinQueryWrapper<T>)typedThis;
    }

}
